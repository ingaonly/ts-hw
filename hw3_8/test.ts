/// <reference path="./string-length.ts" />


const minValidator: Validation.StringValidator = new Validation.MinValidator(5);
 console.log(minValidator.isAcceptable('abc'));                      // false
console.log(minValidator.isAcceptable('abcdef'));                   // true
 const maxValidator: Validation.StringValidator = new Validation.MaxValidator(5);
console.log(maxValidator.isAcceptable('abc'));                     // true
console.log(maxValidator.isAcceptable('abcdef'));                  // false