// Создайте файл string-length.ts, внутри опишите namespace Validation с классом MinValidator,
// * который имплементирует интерфейс StringValidator и реализует проверку на минимальную длину
// * строки.
// *
// * В файле string-length.ts, в namespace Validation опишите класс  MaxValidator,
// * который имплементирует интерфейс StringValidator и реализует проверку на максимальную дину
// * строки.

/// <reference path="./validation.ts" />
namespace Validation{
    export class MinValidator implements StringValidator{
        private minLength: number;
       constructor(minLength:number) {
           this.minLength=minLength;
       }

        isAcceptable(p1: string): boolean {
           if(p1.length>=this.minLength){
               return true;
           }
            return false;
        }
    }
    export class MaxValidator implements StringValidator{
        private maxLength: number;
        constructor(maxLength:number) {
            this.maxLength=maxLength;
        }

        isAcceptable(p1: string): boolean {
            if(p1.length<=this.maxLength){
                return true;
            }
            return false;
        }
    }
}
