// В файле validation.ts опишите namespace Validation c интерфейсом StringValidator внутри которого есть
// * метод isAcceptable, он принимает на вход строку и возвращает boolean.

namespace Validation {
   export  interface StringValidator{
        isAcceptable(p1:string):boolean;
    }
}

