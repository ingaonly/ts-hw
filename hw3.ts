/**
 * Задание 1 - Напишите декоратор @LogStaticProps для класса HeaderComponent со следующей
 * сигнатурой вызова:
 *
 * @LogStaticProps
 * class HeaderComponent {
 *   static title: string = 'My Title';
 *   static subtitle: string = 'My subtitle';
 * }
 *
 * который выведет в консоль значения свойств HeaderComponent.title и HeaderComponent.subtitle
 *
 * Сборка:
 * tsc --experimentalDecorators -w index.ts
 */


// function LogStaticProps(target:any){
//     console.log(target.title);
//     console.log(target.subtitle);
// }
// class ComponentT {
//     static title: string;
//     static subtitle: string;
// }
//
// function LogStaticPropsT<T extends typeof ComponentT>(target:T){
//     console.log(target.title);
//     console.log(target.subtitle);
// }
//
// @LogStaticPropsT
// class HeaderComponent {
//       static title: string = 'My Title';
//       static subtitle: string = 'My subtitle';
// }

/**
 * Задание 2 - Напишите декоратор @LogStaticProps для класса HeaderComponent со следующей
 * сигнатурой вызова:
 *
 * @LogStaticProps({
 *    showLogs: true
 * })
 * class HeaderComponent {
 *   static p1: string = 'V1';
 *   static p2: string = 'V1';
 *   static p3: string = 'V1';
 *   static p4: string = 'V1';
 * }
 *
 * который выведет в консоль значения всех свойств класса к которому декоратор был применен.
 *
 * Добавьте параметр options типа { showLogs: boolean } к декоратору LogStaticProps по флагу которого
 * будет выводится в консоль информация обо всех статических свойствах класса к которому декоратор
 * был применен.
 *
 * По умлочанию showLogs = false
 *
 * Сборка:
 * tsc --experimentalDecorators -w index.ts
 */

class Options{
    showLogs: boolean = false;
}

function LogStaticProps(options: Options={showLogs:false}){
    return function (target:object){
        if (!options.showLogs) {
            return;
        }
        for (let k in target){
            console.log(target[k]);
        }
    }
}

 @LogStaticProps({
       showLogs: true
     })
 class HeaderComponent {
       static p1: string = 'V1';
      static p2: string = 'V2';
       static p3: string = 'V3';
       static p4: string = 'V4';
 }

@LogStaticProps({
    showLogs: false
})
class HeaderComponent2 {
    static p1: string = 'Q1';

}


/**
 * Задание 3 - Напишите декоратор @Component для класса HeaderComponent со следующей
 * сигнатурой вызова:
 *
 * @Component({
 *   template: `
 *     <h1>{{title}}</h1>
 *     <h2>{{subtitle}}</h2>
 *   `,
 *   selector: '.app-header-component'
 * })
 * class HeaderComponent {
 *   static title: string = 'My Title';
 *   static subtitle: string = 'My subtitle';
 * }
 *
 * bootstrap([HeaderComponent]);
 *
 * <div class="app-header-component"></div>
 *
 * После выполнения JS в DOM дереве появляется заполненный шаблон:
 * <div class="app-header-component">
 *     <h1>My Title</h1>
 *     <h2>My subtitle</h2>
 * </div>
 *
 * Декоратор @Component принимает на вход набор опций с 2 свойствами:
 * template - шаблон, с переменными, значения которых будут взяты из
 * статических свойств класса компонента;
 * selector - селектор по которому шаблон будет отрисован в DOM дереве.
 *
 * Описать функцию bootstrap, которая принимает на вход массив классов компонентов,
 * получает от них шаблон с подготовленным HTML и помещает в DOM дерево по селектору
 * компонента.
 *
 * Добавить в HTML необходимый селектор:
 * <div class="app-header-component"></div>
 *
 * При вызове функции bootstrap в DOM дереве появляется HTML с проставленными
 * значениями:
 * bootstrap([HeaderComponent]);
 *
 * <div class="app-header-component">
 *     <h1>My Title</h1>
 *     <h2>My subtitle</h2>
 * </div>
 */







/**
 * Задание 4 - Опишите перегрузку функции нахождения длины входящего параметра.
 *
 * getLength('Some string') //? 11
 * getLength(['Some string']) //? 1
 *
 */
function getLength(s: string[]):number;
function getLength(s: string):number;
function getLength(s: string[]|string):number{
    return s.length;
}

console.log(getLength('Some string')); //? 11
console.log(getLength(['Some string'])); //? 1


/**
 * Задание 5 - Опишите перегрузку функции оператора '+' для параметров a и b
 * с типами number и string.
 *
 * add(1, 2) //? 3
 * add('a', 'b') //? 'ab'
 */


function add(p1:string, p2:string):string;
function add(p1:number, p2:number):number;
function add(p1:string|number, p2:string|number):string|number {
    // return p1+p2;
    if (typeof p1=='number' && typeof p2=='number'){
        return p1+p2;
    }
    if (typeof p1=='string' && typeof p2=='string'){
        return p1+p2;
    }
    return;
}

console.log(add(1,2));
console.log(add('a', 'b') );



/**
 * Задание 6 - Опишите класс Hello, который имеет одно свойство message, которое можно указать в конструкторе.
 * Класс имеет метод greet, который принимает строку или массив строк с именем/именами. При вызове
 * функция greet приветствует персонажа/персонажей приветствием message.
 * Используйте перегрузку метода для описания возможных параметров.
 *
 * const hi = new Hello('Hi');
 * hi.greet('Alice');             // 'Hi, Alice!'
 * hi.greet(['John', 'Jim']);     // ['Hi, John!', 'Hi, Jim!']
 */

class Hello{
    message:string;
    constructor(msg:string) {
        this.message=msg;
    }
    greet(p1:string):string;
    greet(p1:string[]):string[];
    greet(p1:string|string[]):string|string[]{
       if (typeof p1=='string'){
           return `${this.message}, ${p1}!`;
       }
        if (Array.isArray(p1)){
            let resArr:string[]=[];
            for(let i=0; i<p1.length; i++){
                resArr.push(`${this.message}, ${p1[i]}!`)
            }
            return resArr;
        }
    }
}

const hi = new Hello('Hi');
console.log(hi.greet('Alice'));             // 'Hi, Alice!'
console.log(hi.greet(['John', 'Jim']));     // ['Hi, John!', 'Hi, Jim!']


/**
 * Задание 7 - Опишите перегрузку функции оператора '+' для параметров a и b
 * с типами number и string.
 *
 * add(1, 2) //? 3
 * add('a', 'b') //? 'ab'
 */


function add1(p1:string, p2:string):string;
function add1(p1:number, p2:number):number;
function add1(p1:string|number, p2:string|number):string|number {
    // return p1+p2;
    if (typeof p1=='number' && typeof p2=='number'){
        return p1+p2;
    }
    if (typeof p1=='string' && typeof p2=='string'){
        return p1+p2;
    }
    return;
}

console.log(add1(1,2));
console.log(add1('a', 'b') );



/**
 * Задание 8 - В файле validation.ts опишите namespace Validation c интерфейсом StringValidator внутри которого есть
 * метод isAcceptable, он принимает на вход строку и возвращает boolean.
 *
 * Создайте файл string-length.ts, внутри опишите namespace Validation с классом MinValidator,
 * который имплементирует интерфейс StringValidator и реализует проверку на минимальную дину
 * строки.
 *
 * В файле string-length.ts, в namespace Validation опишите класс  MaxValidator,
 * который имплементирует интерфейс StringValidator и реализует проверку на максимальную дину
 * строки.
 *
 * Создайте файл test.ts и протестируйте валидаторы:
 *
 * const minValidator: Validation.StringValidator = new Validation.MinValidator(5);
 * minValidator.isAcceptable('abc');                      // false
 * minValidator.isAcceptable('abcdef');                   // true
 *
 * const maxValidator: Validation.StringValidator = new Validation.MaxValidator(5);
 * maxValidator.isAcceptable('abc');                     // true
 * maxValidator.isAcceptable('abcdef');                  // false
 *
 * Структрура файлов:
 * validation
 *      - validation.ts
 *      - string-length.ts
 *      - test.ts
 *
 * Импорт множественных неймспейсов через /// directive
 * /// <reference path="./string-length.ts" />
 *
 * Сборка:
 * tsc --outFile test.js validation/test.ts
 */

