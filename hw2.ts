/**
 * Задание 1 - Напишите функцию сортировки чисел которая принимает на вход массив
 * строк и тип сортировки ASC или DESC, а возвращает отсортированный в соответствии
 * с типом массив
 *
 * (arr: string[], order: Order) => string[]
 *
 * Использовать: enum
 */

enum SortOrder {
    ASC = 1,
    DESC = -1,
}

function sortNumbers(arr: string[], order: SortOrder): string[] {
    arr.sort(function (a: string, b: string): number {
        if (a > b) {
            if (order == SortOrder.ASC) {
                return 1;
            } else { // SortOrder.DESC
                return -1;
            }
        }
        if (a < b) {
            if (order == SortOrder.ASC) {
                return -1;
            } else { // SortOrder.DESC
                return 1;
            }
        }
        return 0;
    });
    return arr;
}

console.log(sortNumbers(['a', 'b', 'g', '.'], SortOrder.ASC))
console.log(sortNumbers(['1', '10', '3', '4'], SortOrder.ASC))

/**
 * Задание 2 - Напишите функцию которая генерирует пустой объект организации в зависимости
 * от типа организационной собственности ИП или ООО
 *
 * getOrganization(type) => Organization
 *
 * Организация имеет следующие поля:
 *  - ИНН
 *  - КПП (только для ООО)
 *  - ОКПО
 *  - Наименование
 *  - Тип
 *
 *  Использовать: enum
 */

enum OrganizationType {
    PE = 'ИП',
    LLC = 'ООО',
}

interface IOrganization {
    inn: string,
    kpp?: string,
    okpo: string,
    name: string,
    type: OrganizationType,
}

function getOrganization(type: OrganizationType): IOrganization {
    if (type == OrganizationType.PE) {
        const pe: IOrganization = {
            inn: '',
            okpo: '',
            name: '',
            type: OrganizationType.PE,
        }
        return pe;
    } else {
        const llc: IOrganization = {
            inn: '',
            kpp: '',
            okpo: '',
            name: '',
            type: OrganizationType.LLC,
        }
        return llc;
    }
}

console.log(getOrganization(OrganizationType.PE));
console.log(getOrganization(OrganizationType.LLC));


/**
 * Задание 3 - Напишите тип для описания координат x и y, напишите тип прямоугольник
 * который пересекатся с типом координат и описывает ширину и длину. Опишите тоже
 * самое через интерфейсы. Почему для типов в данном случае нельзя использовать объединение ?
 */
//потому что объединение - это или то, или это. Пересечение - это и то, и это.

type TPoint = {
    x: number;
    y: number;
}

type TRectangle = TPoint & {
    a: number;
    b: number;
}

interface IPoint {
    x: number;
    y: number;
}

interface IRectangle extends IPoint {
    a: number;
    b: number;
}


/**
 * Задание 4 - Напишите тип для описания данных пришедших с сервера и возможный случай ошибки.
 * Успешный:
 * {
 *    success: true,
 *    data: { firstName: 'Oleg' }
 * }
 * Произошла ошибка:
 * {
 *    success: false,
 *    errors: [{message: 'Ошибка доступа'}]
 * }
 */

interface IData {
    firstName: string;
}

interface IResponseData {
    data: IData;
}

interface IErrorMessage {
    message: string;
}

interface IResponseErrors {
    errors: IErrorMessage[];
}


interface IResponseStatus {
    success: boolean;
}

type IResponse = IResponseStatus & (IResponseData | IResponseErrors);

const responseSuccess: IResponse = {
    success: true,
    data: {
        firstName: 'foo'
    }
};

const responseErrors: IResponse = {
    success: false,
    errors: [{message: 'Ошибка доступа'}]
}

console.log(responseSuccess);
console.log(responseErrors);

// ----=====II never и проверка на полноту=====---- //

/**
 * Задание 5 - Напишите функцию которая в зависимости от типа окружения
 * выводит одну из возможных строк: Продакшн, Тестовое окружение, Девелоп,
 * Типы окружений: prod, test, dev
 * Позаботтесь о проверки на полноту.
 *
 * Добавить типы Стейджинг и Найтли. Убедить что в TS появилась ошибка после проверки
 * на полноту из-за необработанных случаев.
 * Типы окружений: staiging, nightly
 *
 * Использовать тип пуcтого множества never
 */

// type Environment = 'prod' | 'test' | 'dev';

type Environment= 'prod'| 'test'| 'dev' | 'staging' | 'nightly';

function checkEnv(env: Environment): string {
    if (env === 'prod') {
        return 'Продакшн';
    }
    if (env === 'test') {
        return 'Тестовое окружение';
    }
    if (env === 'dev') {
        return 'Девелоп';
    }

    const check1: never = env;
}

console.log(checkEnv('prod'));
// console.log(checkEnv('staging'));


// ----=====III Интерфейсы и Обобщения=====---- //
/**
 * Задание 6 - Опишите интерфейс пользователя со следующими свойствами. Создайте подходящий объект.
 *  - firstName (строка) - имя
 *  - secondName (строка) - фамилия
 *  - middleName (строка, необязательное поле) - среднее имя
 */

interface UserData {
    firstName: string;
    secondName: string;
    middleName?: string;
}

const myUser: UserData = {
    firstName: 'Mike',
    secondName: 'Brown',
    middleName: 'Christoffer',
}
console.log(myUser);


/**
 * Задание 7 - Опишите интерфейс BaseEntity:
 *  - id (T) - идентификатор пользователя
 *  - addedAt (Дата) - время создания сущности
 *  - updatedAt (Дата) - время обновления сущности
 *  - addedBy (строка) - кто добавил
 *  - updatedBy (строка) - кто последний раз обновил
 *
 * Опишите интерфейс User который расширяет тип BaseEntity с типом string
 * и полями:
 * - firstName: string;
 * - lastName: string;
 *
 * Опишите интерфейс Post который расширяет тип BaseEntity с типом number
 * и полями:
 * - title: string;
 * - authorId: number;
 */

interface BaseEntity<T> {
    id: T,
    addedAt: Date,
    updatedAt: Date,
    addedBy: string,
    updatedBy: string,
}

interface User extends BaseEntity<string> {
    firstName: string,
    lastName: string,
}

interface Post extends BaseEntity<number> {
    title: string,
    authorId: number,
}


/**
 * Задание 8 - Напишите тип для описания данных пришедших с сервера и возможный случай ошибки
 * для получения одиночной сущности и массива данных.
 * Успешный:
 * {
 *    success: true,
 *    data: T
 * }
 * Произошла ошибка:
 * {
 *    success: false,
 *    errors: [{message: 'Ошибка доступа'}]
 * }
 * и
 * Успешный:
 * {
 *    success: true,
 *    data: T[]
 * }
 * Произошла ошибка:
 * {
 *    success: false,
 *    errors: [{message: 'Ошибка доступа'}]
 * }
 *
 * Применить обобщение для типа User с полями id, firstName, lastName
 *
 * Использовать: дженерики
 */

type ApiResponseError = {
    message: string,
}

type ListApiResponse<T> = {
        success: true,
        data: T[]
    }
    | {
    success: false,
    errors: ApiResponseError[]
}

type ApiResponse<T> = {
        success: true,
        data: T,
    }
    | {
    success: false,
    errors: ApiResponseError[]
}

type UserApi ={
    id: number,
    firstName: string,
    lastName:string
}

type UserApiResponse= ApiResponse<UserApi>;
type UserListApiResponse=ListApiResponse<UserApi>;

// ----=====IV ООП=====---- //
/**
 * Задание 9 - Опишите интерфейс пользователя посложнее.
 *
 * Пользователь, как все сущности системы полученные из базы данных имеет мета-свойства базовой
 * сущности BaseEntity:
 *  - id (T) - идентификатор пользователя
 *  - addedAt (Q) - время создания сущности
 *  - updatedAt (Q) - время обновления сущности
 *  - addedBy (P) - кто добавил
 *  - updatedBy (P) - кто последний раз обновил
 *
 * Свойства интерфейса User + BaseEntity (id: string, даты - ISOString, addedBy и updatedBy типа string ) :
 *  - roles (массив типа Role) - список ролей пользователя
 *  - firstName (строка) - имя
 *  - secondName (строка) - фамилия
 *  - middleName (строка, необязательное поле) - среднее имя
 *  - isAdmin (логическое, только для чтения) - является ли пользователь администратором
 *
 * Интферфейс Role также имеет все базовые свойства интерфейса BaseEntity
 * (id: number, даты - Date, addedBy и updatedBy типа string ) и свои:
 *  - name (строка) - наименование роли
 */

interface BaseEntityT<T, Q, P> {
    id: T,
    addedAt: Q,
    updatedAt: Q,
    addedBy: P,
    updatedBy: P,
}

interface UserT extends BaseEntityT<string, string, string >{
    roles:Role[],
    firstName: string,
    lastName:string,
    middleName?:string,
    readonly isAdmin:boolean,
}

interface Role extends BaseEntityT<number, Date, string>{
    name:string,
}