// ----=====Базовые типы, функции, литералы, обобщения, списки=====---- //

/**
 * Задание 1 - Напишите функцию которая принимает на вход год
 * и возвращает век. Необходимо аннотировать все переменные.
 *
 * 1705 => 18
 * 1900 => 19
 * 1601 => 17
 * 2000 => 20
 */

function yearToCentury(year: number): number {
    return Math.ceil(year / 100);
}
console.log(yearToCentury(2000));


/**
 * Задание 2 - Напишите функцию которая принимает на вход строку
 * и возвращает ее инвертированное представление. Необходимо
 * аннотировать все переменные.
 *
 * 'world' => 'dlrow'
 * 'word'  => 'drow'
 */

function reverse(word: string): string {
    let reverseWord: string = '';
    for (let i: number = 0; i < word.length; i++) {
        reverseWord = word[i] + reverseWord;
    }
    return reverseWord
}

console.log(reverse('drow'));

function reverseString(word: string): string {
    return word.split("").reverse().join("");
}
console.log(reverseString("hello"));


/**
 * Задание 3 - Напишите функцию которая принимает на вход массив
 * чисел и возвращает сумму положительных элементов. Необходимо
 * аннотировать все переменные.
 *
 * [1, -4, 7, 12] => 1 + 7 + 12 = 20
 */

function sumPositiveNumbers(array: number[]): number {
    let sum: number = 0;
    for (let i: number = 0; i < array.length; i++) {
        if (array[i] > 0) {
            sum += array[i]
        }
    }
    return sum;
}

console.log(sumPositiveNumbers([1, -4, 7, 12]));

/**
 * Задание 4 - Напишите функцию которая принимает на вход строку
 * и считает количество гласных в ней (a, e, i, o, u).
 *
 * 'I like coding in TypeScript.' => 8
 */

function getCountOfVowels(str: string): number {
    str = str.toLocaleLowerCase();
    let res: number = 0;
    const engVowels: string[] = ['e', 'u', 'i', 'o', 'a'];
    for (let i = 0; i < str.length; i++) {
        if (engVowels.includes(str[i])) {
            res++
        }
    }
    return res;
}

console.log(getCountOfVowels('I like coding in TypeScript.'));

/**
 * Задание 5 - Напишите функцию которая принимает на вход одну строну
 * ДНК в виде массива строк, состоящих только из символов A,T,G,C и
 * возвращает ее и дополняющую сторону в виде кортежа.
 *
 * В цепочках ДНК символы «А» и «Т» дополняют друг друга, как «С» и «G».
 * Ваша функция получает одну сторону ДНК. Нить ДНК никогда не бывает
 * пустой.
 *
 * Необходимо аннотировать все переменные.
 *
 * [A,T,G,C] => [ [A,T,G,C], [T,A,C,G] ]
 * [G,T,A,T] => [ [G,T,A,T], [C,A,T,A] ]
 * [A,A,A,A] => [ [A,A,A,A], [T,T,T,T] ]
 */


function dna(arr: string[]): string[][] {
    const manyArrStr: string[][] = [];
    manyArrStr.push(arr);
    const arrMirror: string[] = [];
    for (let i: number = 0; i < arr.length; i++) {
        switch (arr[i]) {
            case 'A':
                arrMirror.push('T');
                break;
            case 'T':
                arrMirror.push('A');
                break;
            case 'G':
                arrMirror.push('C');
                break;
            case 'C':
                arrMirror.push('G');
                break;
        }
    }
    manyArrStr.push(arrMirror);
    return manyArrStr
}
console.log(dna(['A', 'T', 'G', 'C']));

/**
 * Задание 6 - Напишите функцию которая принимает на вход массив пользователей
 * с полями username, status, lastActivity и возвращает отчет по тому кто из
 * них online, offline или ушел (away).
 *
 * У вас есть приложение для группового чата. Вы хотите показать своим пользователям,
 * кто из их друзей онлайн и доступен для чата. Получив на вход массива объектов,
 * содержащих имена пользователей, статус и время с момента последней активности
 * (в минутах), создайте функцию для определения того, кто находится в сети,
 * в автономном режиме и вне его. Если кто-то находится в сети, но его
 * последняя активность была более 10 минут назад, он считается
 * отсутствующим.
 *
 * Необходимо аннотировать все переменные.
 *
 * [{
 *   username: 'David',
 *   status: 'online',
 *   lastActivity: 10
 * }, {
 *   username: 'Lucy',
 *   status: 'offline',
 *   lastActivity: 22
 * }, {
 *   username: 'Bob',
 *   status: 'online',
 *   lastActivity: 104
 * }] => {
 *   online: ['David'],
 *   offline: ['Lucy'],
 *   away: ['Bob']
 * }
 */

enum ChatUserStatus {
    Online = "online",
    Offline = "offline",
    Away = "away"
}

class ChatUser {
    username: string;
    status: string;
    lastActivity: number;
}

interface ChatUserResultStatuses {
    online: string[];
    offline: string[];
    away: string[];
}

function getChatUserStatus(users: ChatUser[]): ChatUserResultStatuses {
    const online: string[] = [];
    const offline: string[] = [];
    const away: string[] = [];
    for (let i: number = 0; i < users.length; i++) {
        if (users[i].status == 'offline') {
            offline.push(users[i].username);
        } else if (users[i].status == 'online' && users[i].lastActivity <= 10) {
            online.push(users[i].username);
        } else {
            away.push(users[i].username);
        }
    }


    const result: ChatUserResultStatuses = {
        online: online,
        offline: offline,
        away: away
    }
    return result;
}

console.log(getChatUserStatus([{
    username: 'David',
    status: 'online',
    lastActivity: 10
}, {
    username: 'Lucy',
    status: 'offline',
    lastActivity: 22
}, {
    username: 'Bob',
    status: 'online',
    lastActivity: 104
}]));


/**
 * Задание 7 - Напишите функцию которая принимает на вход массив с примитивными
 * типами данных и возвращает кортеж в котором показано сколько раз встречается
 * каждое значение из входного массива.
 *
 * Необходимо аннотировать все переменные.
 *
 * ['Peter', 'Anna', 'Rose', 'Peter', 'Peter', 'Anna'] => [["Anna", 2], ["Peter", 3], ["Rose", 1]]
 * [1, 10, 12, 2, 1, 10, 2, 2] => [[1, 2], [2, 3], [10, 2], [12, 1]]
 * [true, false, true] => [[true, 2], [false, 1]]
 */

function myCount(arr: any[]): [string, number][] {
    const arrResult: [any, number][] = [];
    const map = new Map();

    for (let i: number = 0; i < arr.length; i++) {
        if (map.has(arr[i])) {
            let count: number = map.get(arr[i])
            count++;
            map.set(arr[i], count)
        } else {
            map.set(arr[i], 1)
        }
    }

    map.forEach((value: number, key: any) => {
        const t: [any, number] = [key, value];
        arrResult.push(t);
    })
    return arrResult;
}

console.log(myCount(['Peter', 'Anna', 'Rose', 'Peter', 'Peter', 'Anna']));
console.log(myCount([1, 10, 12, 2, 1, 10, 2, 2]));
console.log(myCount([true, false, true]));

/**
 * Задание 8 - Напишите функцию isUser "защитник типа", которая принимает на вход параметр
 * неизвестного типа и уточняет, что параметр является пользователем - объект с 2
 * обязательными свойствами firstName и age.
 *
 * Использовать:
 *  - is (описание предиката)
 *  - in (проверка на наличие свойства)
 *  - as (отключение типизации для проверки)
 *  - interface (для описания типа User)
 *
 * const user: unknown = JSON.parse('{"firstName": "Alisa", "age": 15}');
 * if (isUser(user)) {
 *    console.log(user.firstName, user.age);
 * }
 */

interface UserGuard {
    firstName: string,
    age: number
}


function isUserGuard(user: unknown): user is UserGuard { //function isUserGuard(user: unknown):boolean
    if (typeof user !== 'object') {
        return false;
    }

    if (!('firstName' in user)) {
        return false;
    }

    if (!('age' in user)) {
        return false;
    }

    if (typeof (user as UserGuard).firstName !== 'string') {
        return false;
    }

    if (typeof (user as UserGuard).age !== 'number') {
        return false;
    }

    return true;
}

console.log(isUserGuard({}))
console.log(isUserGuard('user'))
console.log(isUserGuard({age: '12', firstName: 'h'}))
console.log(isUserGuard({age: 45, firstName: 12}))
console.log(isUserGuard({age: 12, firstName: 'h'}))

// ----=====Интерфейсы и классы=====---- //

/**
 * Задание 9 - Опишите интерфейс пользователя со следующими свойствами. Создайте подходящий объект.
 *  - firstName (строка) - имя
 *  - secondName (строка) - фамилия
 *  - middleName (строка, необязательное поле) - среднее имя
 */

interface IUser {
    firstName: string;
    secondName: string;
    middleName?: string;
}

const user1: IUser = {
    firstName: "Mike",
    secondName: "Brown"
}

const user2: IUser = {
    firstName: "Иван",
    secondName: "Белый",
    middleName: "Иванович"
}

console.log(user1, user2);

/**
 * Задание 10 - Опишите интерфейс пользователя посложнее.
 *
 * Пользователь, как все сущности системы полученные из базы данных имеет мета-свойства базовой
 * сущности BaseEntity:
 *  - id (строка) - идентификатор пользователя
 *  - addedAt (Дата) - время создания сущности
 *  - updatedAt (Дата) - время обновления сущности
 *  - addedBy (строка) - кто добавил
 *  - updatedBy (строка) - кто последний раз обновил
 *
 * Свойства интерфейса User:
 *  - roles (массив типа Role) - список ролей пользователя
 *  - firstName (строка) - имя
 *  - secondName (строка) - фамилия
 *  - middleName (строка, необязательное поле) - среднее имя
 *  - isAdmin (логическое, только для чтения) - является ли пользователь администратором
 *
 * Интерфейс Role также имеет все базовые свойства интерфейса BaseEntity и свои:
 *  - name (строка) - наименование роли
 */

interface BaseEntity {
    id: string;
    addedAt: Date;
    updatedAt: Date;
    addedBy: string;
    updatedBy: string;
}

interface Role extends BaseEntity {
    name: string;
}

interface IUserExt extends BaseEntity {
    roles: Role[];
    firstName: string;
    secondName: string;
    middleName?: string;
    readonly  isAdmin: boolean;
}


/**
 * Задание 11 - Опишите следующие геометрические фигуры используя ООП:
 *  - Точка
 *      P = 0
 *      S = 0
 *  - Круг
 *      P = 2 * r * π
 *      S = π × r ^ 2
 *  - Квадрат
 *      P = 4 * a
 *      S = a ^ 2
 *  - Прямоугольник
 *      P = 2 * a + 2 * b
 *      S = a * b
 *
 * Все фигуры имеют координаты и возможность получить информацию о периметре и площади.
 *
 * Необходимо аннотировать все переменные, использовать имплементацию интерфейсов, наследование,
 * переопределение методов.
 *
 * Компоненты:
 *  - Интерфейс Shape (фигура)
 *  - Класс Dot (точка)
 *  - Класс Circle (круг)
 *  - Класс Squere (квадрат)
 *  - Класс Rectangle (прямоугольник)
 *
 * Методы:
 *  - calcArea()
 *  - calcPerimeter()
 *
 * Свойства:
 *  - x
 *  - y
 *  - r
 *  - a
 *  - b
 */

interface Shape {
    calcArea(): number,

    calcPerimeter(): number,
}

class Dot implements Shape {
    x: number;
    y: number

    calcArea(): number {
        return 0;
    }

    calcPerimeter(): number {
        return 0;
    }
}

class Circle implements Shape {
    x: number;
    y: number;
    r: number;

    calcArea(): number {
        return Math.PI * (this.r ** 2);
    }

    calcPerimeter(): number {
        return 2 * this.r * Math.PI;
    }
}

class Squere implements Shape {
    x: number;
    y: number;
    a: number;

    calcArea(): number {
        return this.a ** 2;
    }

    calcPerimeter(): number {
        return 4 * this.a;
    }
}

class Rectangle implements Shape {
    x: number;
    y: number;
    a: number;
    b: number;

    calcArea(): number {
        return this.a * this.b;
    }

    calcPerimeter(): number {
        return (2 * this.a) + (2 * this.b);
    }
}

