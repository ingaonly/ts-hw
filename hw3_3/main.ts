/**
 * Задание 3 - Напишите декоратор @Component для класса HeaderComponent со следующей
 * сигнатурой вызова:
 *
 * @Component({
 *   template: `
 *     <h1>{{title}}</h1>
 *     <h2>{{subtitle}}</h2>
 *   `,
 *   selector: '.app-header-component'
 * })
 * class HeaderComponent {
 *   static title: string = 'My Title';
 *   static subtitle: string = 'My subtitle';
 * }
 *
 * bootstrap([HeaderComponent]);
 *
 * <div class="app-header-component"></div>
 *
 * После выполнения JS в DOM дереве появляется заполненный шаблон:
 * <div class="app-header-component">
 *     <h1>My Title</h1>
 *     <h2>My subtitle</h2>
 * </div>
 *
 * Декоратор @Component принимает на вход набор опций с 2 свойствами:
 * template - шаблон, с переменными, значения которых будут взяты из
 * статических свойств класса компонента;
 * selector - селектор по которому шаблон будет отрисован в DOM дереве.
 *
 * Описать функцию bootstrap, которая принимает на вход массив классов компонентов,
 * получает от них шаблон с подготовленным HTML и помещает в DOM дерево по селектору
 * компонента.
 *
 * Добавить в HTML необходимый селектор:
 * <div class="app-header-component"></div>
 *
 * При вызове функции bootstrap в DOM дереве появляется HTML с проставленными
 * значениями:
 * bootstrap([HeaderComponent]);
 *
 * <div class="app-header-component">
 *     <h1>My Title</h1>
 *     <h2>My subtitle</h2>
 * </div>
 */

class ComponentOptions{
    template:string;
    selector:string;
}

function Component(option: ComponentOptions){
    return function(target:any){
        target.getDecoratedData =  ()=>{
            for (let key in target){
                option.template = option.template.replace(`{{${key}}}`, target[key]);
            }
            return option
        }

    }
}



 @Component({
      template: `
     <h1>{{title}}</h1>
      <h2>{{subtitle}}</h2>
   `,
       selector: '.app-header-component'
     })
 class HeaderComponent {
       static title: string = 'My Title';
       static subtitle: string = 'My subtitle';
      static getDecoratedData():ComponentOptions{
           return new ComponentOptions();
      }
 }

// abstract class AHeaderComponent {
//     static getDecoratedData(): ComponentOptions {
//         return;
//     };
// }

function bootstrap(arr:any[]){
    arr.map(function (el){
        const opt = el.getDecoratedData();
        const div = document.querySelector(opt.selector)
        div.innerHTML=opt.template;
    });
        // for (let i=0;i<arr.length;i++){
        //     const opt = arr[i].getDecoratedData();
        //     const div = document.querySelector(opt.selector)
        //     div.innerHTML=opt.template;
        // }
}

bootstrap([HeaderComponent]);


